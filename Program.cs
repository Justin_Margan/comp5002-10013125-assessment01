﻿using System;

namespace comp5002_10013125_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {   //Start the program with Clear();
        Console.Clear();

        //Displays a welcome message
        Console.WriteLine("Hello and Welcome to my Store");
        Console.WriteLine("");
        
        //Declaring all the variables
         string name = "";
         var num1 = 0.0;
         var num2 = 0.0;
         var decision = "";

        //Asking the user to input there name
        Console.WriteLine("Please enter your name");
        name = Console.ReadLine();
        Console.WriteLine("");

        //Asking the user to input a number
        Console.WriteLine("Please enter a number with two decimal places");
        num1 = double.Parse (Console.ReadLine());
        Console.WriteLine("");

        //Asking the user if they want to input anymore numbers
        Console.WriteLine("Do you wish to add another number?");
        Console.WriteLine("Type Y for yes or N for no");
        Console.WriteLine("");

         decision = Console.ReadLine();


               if (decision=="Y")
                    {
                    Console.WriteLine("");
                    Console.WriteLine("Please enter another number with two decimal places.");
                    num1 += double.Parse(Console.ReadLine());
                    Console.WriteLine("");
                    }
                if (decision=="N")
                    {
                        Console.WriteLine("Ok here is your total");
                    }

        Console.WriteLine("");
        Console.WriteLine($"Pre-GST Total: {num1} ");
        Console.WriteLine("");


        Console.WriteLine("");
        Console.WriteLine($"Total Including GST {(num1 * 1.15)} ");
        Console.WriteLine("");

           
            Console.WriteLine("");
            Console.WriteLine("Thank you for shopping at my store.");
            Console.WriteLine("");

        //End the program with blank line and instructions
        Console.ResetColor();
        Console.WriteLine();
        Console.WriteLine("Press <Enter> to quit the program");
        Console.ReadKey();
           

        }
    }
}
